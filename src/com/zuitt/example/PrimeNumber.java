package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class PrimeNumber {

    public static void main(String[] args) {

        int[] primeNumbers = new int[5];
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        Scanner indexScanner = new Scanner(System.in);

        System.out.println("Enter a number from 0-4");
        int index = indexScanner.nextInt();

        switch (index){
            case 0:
                System.out.println("The first prime number is: " +primeNumbers[0]);
                break;
            case 1:
                System.out.println("The second prime number is: " +primeNumbers[1]);
                break;
            case 2:
                System.out.println("The third prime number is: " +primeNumbers[2]);
                break;
            case 3:
                System.out.println("The fourth prime number is: " +primeNumbers[3]);
                break;
            case 4:
                System.out.println("The first prime number is: " +primeNumbers[4]);
                break;
            default:
                System.out.println("Invalid Input. ");
        }



        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: "+friends);


        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consist of: "+ inventory);
    }


}
